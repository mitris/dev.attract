<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'user_id',
        'body',
    ];

    protected $visible = [
        'title',
        'slug',
        'body',
        'created_at',
        'user',
        'comments',
        'comments_count',
    ];

//    protected $with = [
//        'user',
//        'comments',
//    ];
//
//    protected $withCount = [
//        'comments',
//    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment')->orderByDesc('created_at');
    }

    public function scopeMostCommented($query, $limit = 10)
    {
        return $query->withCount('comments')->orderByDesc('comments_count')->limit($limit);
    }
}
