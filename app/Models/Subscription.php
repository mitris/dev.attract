<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'subscriber_id',
    ];

    protected $visible = [
        'user_id',
        'subscriber_id',
        'created_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User')->withDefault();
    }

    public function subscriber()
    {
        return $this->belongsTo('App\Models\User', 'id', 'subscriber_id')->withDefault();
    }
}
