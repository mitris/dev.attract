<?php

namespace App\Http\Controllers\Api;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'news_id' => 'required|numeric|exists:App\Models\News,id',
            'reply_id' => 'nullable|numeric',
            'body' => 'required',
        ]);

        if ($request->has('reply_id')) {
            $reply = Comment::findOrFail($request->get('reply_id'));
        }

        $comment = new Comment();
        $comment->news_id = $request->get('news_id');
        $comment->user_id = $request->user()->id;
        if ($reply && !$reply->reply_id) {
            $comment->reply_id = $reply->reply_id;
        }
        $comment->body = $request->get('body');
        $comment->save();

        return response()->json($comment);
    }
}
