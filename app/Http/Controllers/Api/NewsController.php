<?php

namespace App\Http\Controllers\Api;

use App\Models\News;
use App\Notifications\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;

class NewsController extends Controller
{
    public function index()
    {
        $query = News::orderByDesc('created_at')->withCount('comments');

        return response()->json($query->paginate());
    }

    public function most_commented(Request $request)
    {
        $query = News::mostCommented($request->get('limit'));

        return response()->json($query->get());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $news = new News();
        $news->user_id = $request->user()->id;
        $news->title = $request->get('title');
        $news->title = \Str::slug($request->get('title'));
        $news->body = $request->get('body');
        $news->save();

        // send email to subscribers
        dd($request->user()->subscribers->pluck('email'));
//        Notification::send($request->user()->subscribers->pluck('email'), new Subscription($news));
    }

    public function show(News $news)
    {
        return response()->json($news->with('comments')->firstOrFail());
    }

    public function update(Request $request, $news)
    {
        if ($news->user_id == $request->user()->id) {
            $data = $request->all();
            $data['slug'] = \Str::slug($request->get('title'));
            $news->update();

            return $news;
        } else {
            return abort(404, 'Not found');
        }
    }

    public function destroy(Request $request, News $news)
    {
        if ($news->user_id == $request->user()->id) {
            $news->delete();

            return $news;
        } else {
            return abort(404, 'Not found');
        }
    }
}
