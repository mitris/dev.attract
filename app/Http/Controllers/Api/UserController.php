<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function registration(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:App\Models\User',
            'password' => 'required|confirmed',
        ]);

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        return response()->json([
            'message' => 'User registered',
            'user' => $user,
        ]);
    }

    public function profile(Request $request)
    {
        return response()->json($request->user()->subscribers);
    }
}
