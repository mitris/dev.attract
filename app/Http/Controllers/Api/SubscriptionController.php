<?php

namespace App\Http\Controllers\Api;

use App\Models\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    public function toggle(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:App\Models\User,id',
        ]);

        $subscription = Subscription::where([
            'user_id' => $request->get('user_id'),
            'subscriber_id' => $request->user()->id,
        ])->first();

        // add cache

        if ($subscription) {
            $subscription->delete();
        } else {
            $subscription = Subscription::create([
                'user_id' => $request->get('user_id'),
                'subscriber_id' => $request->user()->id,
            ]);
        }

        return response()->json([
            'message' => $subscription->exists() ? 'Subscription created' : 'Subscription deleted',
            'subscription' => $subscription->exists() ? $subscription : null,
        ]);
    }
}
