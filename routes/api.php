<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/auth/token', 'AuthController@token')->name('auth.token');
Route::post('/auth/token/revoke', 'AuthController@token_revoke')->name('auth.token.revoke');

Route::post('/user/registration', 'UserController@registration')->name('user.registration');

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user/profile', 'UserController@profile')->name('user.profile');

    Route::group(['prefix' => 'news', 'as' => 'news.'], function() {
        Route::get('/', 'NewsController@index')->name('index');
        Route::get('/show/{news}', 'NewsController@show')->name('show');
        Route::post('/store', 'NewsController@store')->name('store');
        Route::put('/update/{news}', 'NewsController@update')->name('update');
        Route::delete('/destroy/{news}', 'NewsController@destroy')->name('destroy');
        Route::get('/most_commented', 'NewsController@most_commented')->name('most_commented');
    });

    Route::group(['prefix' => 'comment', 'as' => 'comment.'], function() {
        Route::post('/store', 'CommentController@store')->name('store');
    });

    Route::group(['prefix' => 'subscription', 'as' => 'subscription.'], function() {
        Route::post('/toggle', 'SubscriptionController@toggle')->name('toggle');
    });
});

