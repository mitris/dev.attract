<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\News;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Test User',
            'email' => 'test@site.com',
            'password' => bcrypt('password'),
            'email_verified_at' => now(),
        ]);

        User::factory(10)->create();

        News::factory(20)->create();

        Comment::factory(50)->create();

        for ($i = 0; $i < 10; $i++) {
            $subscription = new Subscription();
            $subscription->user_id = rand(1, 10);
            $subscription->subscriber_id = rand(1, 10);
            $subscription->save();
        }
    }
}
